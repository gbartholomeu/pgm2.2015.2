package revisao;

import java.util.Arrays;


public class ManipuladorDeArrays {
    
    /*
     * Retorna um array contendo os elementos de valores até a posição
     * proximoLivre (sem incluí-la).
     */
    public static int[] redimensiona(int[] valores, int proximoLivre) {
        int[] tmp = new int[proximoLivre];
        for (int i = 0; i < tmp.length; i++) {
            tmp[i] = valores[i];
        }
        return tmp;
    }
    
    private static boolean podeAdicionar(int[] valores, int valor, int[] tmp, int proximoLivreEmTmp) {
        for (int j = 0; j < proximoLivreEmTmp; j++) {
            if (valor == tmp[j]) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Exercício 7.
     * Escreva um método chamado removerDuplicatas que, dado um array contendo
     * valores inteiros, retorne o mesmo sem os valores duplicados.
     */
    public static int[] removerDuplicatas(int[] valores) {
        int[] tmp = new int[valores.length];
        /* Variável necessária para controlar a posição em que está o último 
         * elemento útil do array e diferenciar posições livres no array das
         * posições que contém 0 por não terem sido inicializadas. Obs.: quando
         * um array de int é criado, à todas as posições é atribuído o valor 0.
         */
        int proximoLivreEmTmp = 0;
        
        // coloca em tmp[] os elementos (sem duplicatas)
        for (int i = 0; i < valores.length; i++) {
            if (podeAdicionar(valores, valores[i], tmp, proximoLivreEmTmp)) {
                tmp[proximoLivreEmTmp] = valores[i];
                proximoLivreEmTmp++;
            }
        }
        return redimensiona(tmp, proximoLivreEmTmp);
    }
    
    
    /*
     * Retorna a posição do menor elemento a partir da posição i.
     */
    private static int menorAPartirDe(int[] valores, int i) {
        int menor = i;
        for (int j = i; j < valores.length; j++) {
            if (valores[j] < valores[menor]) {
                menor = j;
            }
        }
        return menor;
    }
    
    
    /**
     * Exercício 8.
     * Escreva um método chamado ordenar, que receba como parâmetro um array de
     * inteiros a e retorne um array com o conteúdo de a ordenado.
     */
    public static int[] ordenar(int[] valores) {
        /*
         * Procura o menor elemento do array e coloca-o no começo do mesmo.
         */
        for (int i = 0; i < valores.length; i++) {
            int posicaoDoMenor = menorAPartirDe(valores, i);
            int tmp = valores[i];
            valores[i] = valores[posicaoDoMenor];
            valores[posicaoDoMenor] = tmp;
            
        }
        return valores;
    }

    
    private static int somaLinha(int[][] matriz, int linha) {
        int soma = 0;
        for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
            soma += matriz[linha][coluna];
        }
        return soma;
    }
    
    private static int somaColuna(int[][] matriz, int coluna) {
        int soma = 0;
        for (int linha = 0; linha < matriz.length; linha++) {
            soma += matriz[linha][coluna];
        }
        return soma;
    }


    /**
     * Exercício 9. 
     * Uma matriz quadrada (NxN) de inteiros é dita um "quadrado mágico" quando
     * a soma de cada uma de suas linhas, de suas colunas e de suas diagonais
     * principal e secundária forem iguais.
     */
    public static boolean éQuadradoMágico(int[][] matriz) {
        int somaDiagonalPrimária = 0;
        for (int i = 0; i < matriz.length; i++) {
            somaDiagonalPrimária += matriz[i][i];
        }
        int soma = 0;
        for (int i = 0; i < matriz.length; i++) {
            soma += matriz[i][i];
        }
        if (somaDiagonalPrimária != soma) {
            return false;
        }
        for (int i = 0; i < matriz.length; i++) {
            if (somaLinha(matriz, i) != somaDiagonalPrimária || somaColuna(matriz, i) != somaDiagonalPrimária) {
                return false;
            }
        }
        return true;
    }
    

    public static void main(String[] args) {
        System.out.println("removerDuplicatas funcionando? " + Arrays.equals(removerDuplicatas(new int[] {1, 2, 3, 4, 5, 6, 7}), new int[] {1, 2, 3, 4, 5, 6, 7}) );
        System.out.println("removerDuplicatas funcionando? " + Arrays.equals(removerDuplicatas(new int[] {1, 2, 3, 4, 5, 5, 7}), new int[] {1, 2, 3, 4, 5, 7}) );
        System.out.println("removerDuplicatas funcionando? " + Arrays.equals(removerDuplicatas(new int[] {1, 2, 3, 4, 0, 0, 7}), new int[] {1, 2, 3, 4, 0, 7}) );
        System.out.println("removerDuplicatas funcionando? " + Arrays.equals(removerDuplicatas(new int[] {1, 2, 2, 2, 5, 6, 7}), new int[] {1, 2, 5, 6, 7}) );
        System.out.println("removerDuplicatas funcionando? " + Arrays.equals(removerDuplicatas(new int[] {1, 2, 2, 2, 5, 5, 7}), new int[] {1, 2, 5, 7}) );
        System.out.println("removerDuplicatas funcionando? " + Arrays.equals(removerDuplicatas(new int[] {1, 0}), new int[] {1, 0}) );
        System.out.println("removerDuplicatas funcionando? " + Arrays.equals(removerDuplicatas(new int[] {1}), new int[] {1}) );
        System.out.println("removerDuplicatas funcionando? " + Arrays.equals(removerDuplicatas(new int[] {}), new int[] {}) );
        
        System.out.println("ordenar funcionando? " + Arrays.equals(ordenar(new int[] {9, 4, 3, 5, 0, 2, 8, 1}), new int[] {0, 1, 2, 3, 4, 5, 8, 9}));
        System.out.println("ordenar funcionando? " + Arrays.equals(ordenar(new int[] {0}), new int[] {0}));
        System.out.println("ordenar funcionando? " + Arrays.equals(ordenar(new int[] {-1}), new int[] {-1}));
        System.out.println("ordenar funcionando? " + Arrays.equals(ordenar(new int[] {}), new int[] {}));
        
        int[][] matriz = {
            {8, 0, 7},
            {4, 5, 6},
            {3, 10, 2}
        };
        System.out.println("éQuadradoMágico: " + éQuadradoMágico(matriz));
        matriz[0][0] = 0;
        System.out.println("éQuadradoMágico: " + (éQuadradoMágico(matriz) == false));
        matriz = new int[][] {
            {2, 2},
            {2, 2}
        };
        System.out.println("éQuadradoMágico: " + éQuadradoMágico(matriz));
    }

}
